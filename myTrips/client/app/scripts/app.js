'use strict';

angular.module('mytripsApp', ['ui.router','ngResource','ngDialog'])
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      
    // route for the home page
    .state('app', {
      url:'/',
      views: {
        'header': {
          templateUrl : 'views/header.html',
          controller  : 'HeaderController'          
        },
        'content': {
          templateUrl : 'views/home.html',
          controller  : 'HomeController'
        },
        'footer': {
          templateUrl : 'views/footer.html',
        }
      }
    })

    // route for the trips page
    .state('app.trips', {
      url: 'trips',
      views: {
        'content@': {
          templateUrl : 'views/trips.html',
          controller  : 'TripsController'
        }
      }
    })
  
    $urlRouterProvider.otherwise('/');
  })
;
