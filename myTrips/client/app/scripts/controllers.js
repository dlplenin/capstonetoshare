'use strict';

angular.module('mytripsApp')
.controller('HeaderController', ['$scope', '$state', '$rootScope', function ($scope, $state, $rootScope) {
  $scope.stateis = function(curstate) {
    return $state.is(curstate);  
  };
}])
.controller('TripsController', ['$scope', '$state', '$window', 'tripsFactory', function ($scope, $state, $window, tripsFactory) {
  $scope.message = "Loading ...";

  $scope.submitTrip = function () {
    tripsFactory.save($scope.trip)
    .$promise.then(
      function (response) {
        $state.go($state.current, {}, {reload: true});
        $scope.tripForm.$setPristine();
        $window.location.href = '/';
      },
      function (response) {
        $scope.message = "Error: " + response.status + " " + response.statusText;
      }
    );
  }
}])
.controller('HomeController', ['$scope', '$state', 'tripsFactory', function ($scope, $state, tripsFactory) {
  $scope.showData = false;
  $scope.trips = tripsFactory.query({})
  .$promise.then(
    function (response) {
      $scope.showData = true;
      $scope.trips = response
    },
    function (response) {
      $scope.message = "Error: " + response.status + " " + response.statusText;
    }
  );
}])
;