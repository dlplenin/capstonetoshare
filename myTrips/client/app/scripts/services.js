'use strict';

angular.module('mytripsApp')
.constant("baseURL", "https://localhost:3443/")
.factory('tripsFactory', ['$resource', 'baseURL', function ($resource, baseURL) {
  return $resource(baseURL + "trips/:id", null, {
    'update': {
      method: 'PUT'
    }
  });
}])
.factory('$localStorage', ['$window', function ($window) {
  return {
    store: function (key, value) {
      $window.localStorage[key] = value;
    },
    get: function (key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    remove: function (key) {
      $window.localStorage.removeItem(key);
    },
    storeObject: function (key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function (key, defaultValue) {
      return JSON.parse($window.localStorage[key] || defaultValue);
    }
  }
}])
;