var bodyParser = require('body-parser');
var express = require('express');
var tripRouter = express.Router();
var mongoose = require('mongoose');

var Trips = require('../models/trips');

tripRouter.use(bodyParser.json());

tripRouter.route('/')
.get(function (req, res, next) {
  Trips.find(req.query)
  .exec(function (err, trip) {
    if (err) return next(err);
    res.json(trip);
  });
  })

  .post(function (req, res, next) {
    Trips.create(req.body, function (err, trip) {
        if (err) return next(err);
        console.log('Trip created!');
        var id = trip._id;

        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.end('Added the trip with id: ' + id);
    });
  })

  .delete(function (req, res, next) {
  Trips.remove({}, function (err, resp) {
      if (err) return next(err);
      res.json(resp);
  });
  });

tripRouter.route('/:tripId')
  .get(function (req, res, next) {
  Trips.findById(req.params.tripId)
  .exec(function (err, trip) {
    if (err) return next(err);
    res.json(trip);
  });
  })

  .put(function (req, res, next) {
    Trips.findByIdAndUpdate(req.params.tripId, {
        $set: req.body
    }, {
        new: true
    }, function (err, trip) {
        if (err) return next(err);
        res.json(trip);
    });
  })

  .delete(function (req, res, next) {
    Trips.findByIdAndRemove(req.params.tripId, function (err, resp) {        
      if (err) return next(err);
      res.json(resp);
    });
  });

//  exports.router = tripRouter;
module.exports = tripRouter;