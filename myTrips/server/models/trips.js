var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// Will add the Currency type to the Mongoose Schema types
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

// create a schema
var tripSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  place: {
    type: String,
    required: true
  },
  date: {
    type: String
  },
  budget: {
    type: Currency, min: 0
  },
  notes: {
    type: String
  },
  fun:  {
    type: Number,
    min: 1,
    max: 5,
    required: true
  },
  nextYear: {
    type: Boolean, default: false
  },
  image: {
    type: String
  }
}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Trips = mongoose.model('Trip', tripSchema);

// make this available to our Node applications
module.exports = Trips;